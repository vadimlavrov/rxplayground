//
//  RxTabBarController.swift
//  RxPlayground
//
//  Created by Vadim on 9/10/17.
//  Copyright © 2017 Vadim. All rights reserved.
//

class RxTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let tabOneController: RXTabController1 = RXTabController1.createController(RXTabController1.defaultIdentifier)
        let tabOneBarItem = UITabBarItem(title: "Tab 1", image: nil, selectedImage: nil)
        
        tabOneController.tabBarItem = tabOneBarItem
        
        let tabOasController: RxOasTabViewController = RxOasTabViewController.createController(RxOasTabViewController.defaultIdentifier)
        let tabOasBarItem = UITabBarItem(title: "Oas", image: nil, selectedImage: nil)
        
        tabOasController.tabBarItem = tabOasBarItem
        
        self.viewControllers = [tabOneController, tabOasController]
    }
    
}

extension RxTabBarController: UITabBarControllerDelegate {
    
    
    //    // UITabBarControllerDelegate
    //    func tabBarController(_ tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
    //        print("Selected \(viewController.title!)")
    //    }
}
