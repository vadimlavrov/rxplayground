//
//  UIViewController+Extension.swift
//  QuentiqTracker
//
//  Created by Vadim on 9/10/17.
//  Copyright © 2017 Vadim. All rights reserved.
//

extension UIViewController: DefaultIdentifier {
    
    static func createController <T: UIViewController>(_ stroryboardName: String) -> T  where T: DefaultIdentifier {
        guard let controller = UIStoryboard(name: stroryboardName, bundle:Bundle.main).instantiateViewController(withIdentifier: T.defaultIdentifier) as? T else {
            fatalError("Could not dequeue controller with identifier: \(T.defaultIdentifier) in storyboard: \(stroryboardName)")
        }
        
        return controller
    }
}
