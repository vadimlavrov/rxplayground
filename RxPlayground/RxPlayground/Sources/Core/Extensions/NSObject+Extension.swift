//
//  NSObject+Extension.swift
//  QuentiqTracker
//
//  Created by Vadim on 9/10/17.
//  Copyright © 2017 Vadim. All rights reserved.
//

protocol DefaultIdentifier: class {
    static var defaultIdentifier: String { get }
}

extension DefaultIdentifier where Self: NSObject {
    static var defaultIdentifier: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}
